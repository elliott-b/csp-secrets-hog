# csp-secrets-hog
Secrets-hog searches single local or remove git repository for secrets, digging deep into commit history and branches. 
This is effective at finding secrets accidentally committed in clear text. Can be run against a local repo, or remote URLs

secrets-hog is heavily adapted from trufflehog ( pygit, commits/branch looping, entropy )
Credit: https://github.com/dxa4481/truffleHog

### Install
```
git clone https://gitlab.com/elliott-b/csp-secrets-hog.git 
cd secrets-hog
pip install -r requirements.txt
```

## External VCS auth 
For cloning via https, the following env vars should be set, for each intended use case. Supply a valid user and token
Gitlab
```
GITLAB_USER
GITLAB_TOKEN
```

Github
```
GITHUB_USER
GITHUB_API
```

Bitbucket use: Bitbucket 5.1.2 and later now support API tokens
```
BITBUCKET_USER
BITBUCKET_TOKEN
```

### Use in automated CI/CD build pipeline
Script can be used with "CI flags" in a build system to flag cleartext secrets and notify developer. The current state 
will not return an error code that would fail build, but just returns descriptive comments. A choice here.

##### CI outputs:
Flags to use for CI/CD: `--ci`
Simple pass/fail that shows only files flagged

`--ci_hashes`
Pass fail, but include a list of offending commit hashes for that file. With no flags, script will just output a single 
muted pass/fail line, which is likely not useful unless output is clobbering logs.

Possible outputs:
Pass - "OK: No findings for repo scan."

Flagged files - "[.] WARNING: Potential secrets flagged!"

Example `--ci` output for remote repo (fake):
`
INFO:secretshog: [.] 12 files flagged for possible secrets in gitlab.com:elliott-b/secrets-hog.git!
INFO:secretshog: Flagged: secretshog/keyRegexes.json
INFO:secretshog:     Reason(s): RSA private key, SSH (OPENSSH) private key
INFO:secretshog: Flagged: secretshog/regexes_w_passwords.json
INFO:secretshog:     Reason(s): RSA private key, SSH (OPENSSH) private key
`

### basic scanning on a remote repo
`python secretshog.py --repo_url https://gitlab.com/elliott-b/csp-secrets-hog.git --print_findings`

##### Using SSH key auth
`python secretshog.py --repo_url git@gitlab.com:elliott-b/csp-secrets-hog.git`

## Local repo format
Possible to scan a local git repo using file:/// git syntax
`--repo_url file:///Users/myuser/src/secretshog/`

## full python parse Arguments ( 7/9/2020 ):
```
usage: secretshog.py [-h] [--json] [--do_regex] [--dry_run] [--skip_tests]
                     [--rules RULES] [--do_entropy]
                     [--since_commit SINCE_COMMIT] [--max_depth MAX_DEPTH]
                     [--debug] [--repo_url REPO_URL] [--whitelist_all]
                     [--whitelist_file WHITELIST_FILE]
                     [--max_threads MAX_THREADS] [--save] [--print_findings]
                     [--ci] [--ci_hashes]

Find secretshog hidden in the depths of git.

optional arguments:
  -h, --help            show this help message and exit
  --json                Output in JSON
  --do_regex            Enable high signal regex checks
  --dry_run             Setup repos but dont clone or scan
  --skip_tests          Dont report on test file findings
  --rules RULES         Ignore default regexes and source from json list file
  --do_entropy          Enable entropy checks
  --since_commit SINCE_COMMIT
                        Only scan from a given commit hash
  --max_depth MAX_DEPTH
                        The max commit depth to go back when searching for
                        secretshog
  --debug               Enable debug output
  --repo_url REPO_URL   URL for secret searching
  --whitelist_all       Create file named <input> storing list of findings to
                        whitelist on subsequent runs
  --whitelist_file WHITELIST_FILE
                        File with hashes to skip
  --max_threads MAX_THREADS
                        Thread count for parallel scanning
  --save                Save results to local file
  --print_findings      Print results to screen
  --ci                  Print a short summary for CI PASS/FAIL
  --ci_hashes           Print semi detailed CI PASS/FAIL
```

### Whitelisting for large projects with many false positives
create whitelist strings, and save to a file
```
python secretshog.py --regex --vcs github --parent_project myproject  --whitelist_all whitelist_name
```
skip findings based in a whitelist

```
python secretshog.py --regex --vcs github --parent_project myproject  --whitelist_file whitelist_name --max_depth 8
```

### Contributing
The most configurable item is the regex pattern files. Either password/string regex, or keys
Other updates are welcome  can be done via pull request to master, and for considerable updates, kept in a branch!

##### Major changes to original truffle hog:
- Totally refactored to be class based. Removes dup code
- Print function switched to a data structure (TF was just printing to screen as things showed up ).
    dup findings and whitelisting is easier for large jobs like 200 repos
- Whitelisting functionality via command flags added.
- clear pep 8 violations
- separate print, save functions, and CI output
- Threaded clone function that will fail after specified timeout. 

#### Known issues:
Using some of the original since_commit flags provided by truffleHog, may result in bugs related to first commit skipped


