import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='secretshog',
    version='2.3.3',
    author='Elliott Brun',
    author_email='elliottb303@gmail.com',
    keywords='secrets scanning git history',
    description='Searches through git repositories for high entropy strings, digging deep into commit history.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/elliott-b/csp-secrets-hog',
    packages=setuptools.find_packages(),
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'GitPython == 3.1.0',
        'requests == 2.22.0',
        'PyGithub'
    ],
)
