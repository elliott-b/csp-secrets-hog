#!/usr/bin/env python
# -*- coding: utf-8 -*-
import shutil
import sys
import math
import datetime
import resource
import argparse
import tempfile
import os
import re
import json
import stat
from git import Repo
from git import NULL_TREE
from git import exc
import requests
import logging
from collections import defaultdict
import hashlib
import base64
from billiard.context import Process

logging.basicConfig()
logger = logging.getLogger('secretshog')


class SecretsHog(object):
    t_delay = .5
    ignores = ['node_modules', 'vendor', 'Godeps', 'k8s.io', 'samples', 'Test', 'bower_components', 'contrib',
               'botocore', 'boto3']
    base_64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    hex_chars = "1234567890abcdefABCDEF"
    regex_line_search_terms = ['AWS', 'password']
    debug_break = "\n" + '*' * 80 + "\n"
    # Cancel single repo worker going over 4 GB memory; send debug note. Cap clone time to 5 min for broked repos
    memory_cap = 4000000000
    memory_conv = 1000000
    clone_timeout = 300

    def __init__(self, **kwargs):
        super(SecretsHog, self).__init__()
        self.whitelist_file = kwargs.get('whitelist_file', None)
        self.whitelist_all = kwargs.get('whitelist_all', False)
        self.do_regex = kwargs.get('do_regex', True)
        self.max_depth = kwargs.get('max_depth', 864)
        self.do_entropy = kwargs.get('do_entropy', False)
        self.repo_url = kwargs.get('repo_url', None)
        self.repo_ssh = kwargs.get('repo_ssh', None)
        self.repo_md = kwargs.get('repo_md', None)
        self.project = kwargs.get('project', None)
        self.debug = kwargs.get('debug', False)
        self.output = {"found_issues": [], 'branch_count': 0, 'commit_count': 0}
        self.current_issues = []
        self.repo_issues = None
        self.max_threads = kwargs.get('max_threads', 2)
        self.repo_name = None
        self.tmp_repo = None
        self.repo_token_url = None
        self.rules_file = None
        self.finding_summaries = {}
        self.findings = []
        self.git_user = None
        self.git_api = None
        self.temp_repo_path = None
        self.since_commit = None
        self.output_json = False
        self.security_md_exists = False
        self.repo_size = 0
        self.branch_name = None
        self.commit_hash = None
        self.regexes = {}
        self.active = []
        self.whitelist_in = defaultdict(list)
        self.whitelist_count = 0
        self.repo_hash = []
        self.logger = logging.getLogger('secretshog.SecretsHog')
        self.logger.debug('Creating SecretsHog single repo scan instance class')
        self.git_init()
        self.validate_url()
        self.create_regexes()
        self.env = os.getenv('env', 'prod')
        if self.env == 'dev':
            self.memory_cap = 1000000000
        if self.debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)

    def validate_url(self):
        if not self.repo_url:
            logger.info("No URL supplied for scanning. Try again by supplying 'repo_url' ")
            sys.exit(1)
        # set API tokens from ENV into URLs for automated pull access. Fall back from git
        # Using HTTPS with token can cause token leak in logs where errors occur, or during debug. Default ssh
        if 'https' in self.repo_url:
            if self.git_api and self.git_user:
                self.repo_token_url = self.repo_url.replace('https://',
                                                            'https://' + self.git_user + ':' + self.git_api + '@')
            else:
                logger.debug("No api token located for any URL. Using basic auth")
        # set repo name as url without any github tokens, for printing
        if '@' in self.repo_url:
            self.repo_name = self.repo_url.split('@')[1]
        elif '//' in self.repo_url:
            self.repo_name = self.repo_url.split('//')[1]
        else:
            self.repo_name = self.repo_url

    def git_init(self):
        if 'github' in self.repo_url:
            try:
                self.git_user = os.environ['GITHUB_USER']
                self.git_api = os.environ['GITHUB_API']
            except KeyError:
                logger.info("No api token found for github. Fallback to basic auth fallback, or supply git@ for ssh")
        elif 'bitbucket' in self.repo_url:
            try:
                self.git_user = os.environ['BITBUCKET_USER']
                self.git_api = os.environ['BITBUCKET_TOKEN']
                # b64cred = base64.encodestring('%s:%s' % (un, pw)).replace('\n', '')
                # url_headers = {'Authorization': 'Basic %s' % b64cred}
            except KeyError:
                logger.info("BB API token or creds not found in env var 'BITBUCKET_USER, BITBUCKET_TOKEN'")
        elif 'gitlab' in self.repo_url:
            try:
                self.git_user = os.environ['GITLAB_USER']
                self.git_api = os.environ['GITLAB_TOKEN']
            except KeyError:
                logger.info("No api token found for gitlab. Basic auth fallback, or supply git@ format for ssh cloning")

    def create_regexes(self):
        compiled_rules = {}
        try:
            for rule in regex_patterns:
                compiled_rules[rule] = re.compile(regex_patterns[rule], re.I)
            self.regexes = compiled_rules
        except (IOError, ValueError) as e:
            raise ("Error compiling rules file. Does default 'regexes.json' exist?")

    def regex_check(self, printable_diff, file_path, pc_hash, pc_time, pc_message):
        regex_matches = []
        for key in self.regexes:
            # ciso 4/25/2018 split out diff in lines, to be compatible with ^ beginning of line regex
            if any(do_granular in key for do_granular in self.regex_line_search_terms):
                diff_lines = printable_diff.split('\n')
                for index, line in enumerate(diff_lines):
                    if len(line) < 1024:
                        found_strings = self.regexes[key].findall(line)
                        if found_strings:
                            for found_string in found_strings:
                                multi_line_string = line + '\n' + diff_lines[index + 1]
                                try:
                                    found_diff = printable_diff.replace(printable_diff, multi_line_string)
                                except TypeError:
                                    found_diff = "Possible False positive string %s" % printable_diff
                            regex_matches.append({
                                'date': pc_time,
                                'path': file_path,
                                'branch': self.branch_name,
                                'commit': pc_message,
                                'print_diff': found_diff,
                                'reason': key,
                                'commit_hash': pc_hash
                            })
            else:
                found_strings = self.regexes[key].findall(printable_diff)
                if found_strings:
                    for found_string in found_strings:
                        try:
                            found_diff = printable_diff.replace(printable_diff, found_string)
                        except TypeError:
                            found_diff = "Possible False positive string %s" % printable_diff
                    regex_matches.append({
                        'date': pc_time,
                        'path': file_path,
                        'branch': self.branch_name,
                        'commit': pc_message,
                        'print_diff': found_diff,
                        'reason': key,
                        'commit_hash': pc_hash
                    })
        return regex_matches

    # this likely needs work to return correct string for entropy. Entropy does result in high false positives.
    def find_entropy(self, printable_diff, file_path, pc_hash, pc_time, pc_message):
        strings_found = []
        lines = printable_diff.split("\n")
        for line in lines:
            if "- secure:" in line:
                logger.info("Will ignore ansible encrypted phrase")
                continue
            for word in line.split():
                base64_strings = self.get_strings_of_set(word, self.base_64_chars)
                hex_strings = self.get_strings_of_set(word, self.hex_chars)

                # removed dumb color coding of strings below, leaving string replace as is
                for string in base64_strings:
                    b64_entropy = self.shannon_entropy(string, self.base_64_chars)
                    if b64_entropy > 4.5:
                        strings_found.append(string)
                        printable_diff = printable_diff.replace(string, string)
                for string in hex_strings:
                    hex_entropy = self.shannon_entropy(string, self.hex_chars)
                    if hex_entropy > 3:
                        strings_found.append(string)
                        printable_diff = printable_diff.replace(string, string)
        entropic_diff = None
        if len(strings_found) > 0:
            entropic_diff = {
                'date': pc_time,
                'path': file_path,
                'branch': self.branch_name,
                'commit': pc_message,
                'print_diff': printable_diff,
                'commit_hash': pc_hash,
                'reason': "High Entropy"
            }
        return entropic_diff

    def find_strings(self):
        if not self.clone_git_repo():
            logger.error("Repo clone issue, cancelling strings search. \n")
            return None
        tmp_repo = Repo(self.temp_repo_path)
        self.repo_size = self.check_repo_size(self.temp_repo_path)
        self.repo_issues = []
        total_commits = None
        already_searched = set()

        # workaround for possible situation where commits need to be capped. Trying to search everything, if possible.
        if self.max_depth < 265:
            try:
                total_commits = len(list(tmp_repo.iter_commits()))
            except ValueError:
                logger.info("Total commits attempt broke, will increase max depth")
                total_commits = None
                self.max_depth = 864
        for branch in tmp_repo.remotes.origin.fetch():
            self.branch_name = branch.name.split('/')[1]
            self.output['branch_count'] += 1
            usage = self.get_m_stat()
            logger.debug(f"[.] Branch search: {self.branch_name} MEMORY Use- {usage / self.memory_conv}")
            if usage > self.memory_cap:
                logger.error("HIGH MEMORY: %s Hogs Class is over mem cap %s, and using %s MB. Truncating search! " %
                             (self.repo_url, self.memory_cap, (usage / self.memory_conv)))
                return usage
            try:
                tmp_repo.git.checkout(branch, b=self.branch_name)
            except:
                # already on this branch / usually master
                pass
            prev_commit = None
            for commit in tmp_repo.iter_commits(max_count=self.max_depth):
                since_commit_reached = False
                self.commit_hash = commit.hexsha
                if self.commit_hash == self.since_commit:
                    since_commit_reached = True
                if self.since_commit and since_commit_reached:
                    prev_commit = commit
                    break
                diff_hash = hashlib.md5((str(prev_commit) + str(commit)).encode('utf-8')).digest()
                if not prev_commit:
                    prev_commit = commit
                    continue
                elif diff_hash in already_searched:
                    prev_commit = commit
                    continue
                else:
                    try:
                        current_diff = prev_commit.diff(commit, create_patch=True)
                    except exc.GitCommandError:
                        logger.info("Error on current commit %s in repo %s" % (commit, self.repo_name))
                        logger.debug("Error trying to diff.. commit %s in repo %s" % (commit, self.repo_name))
                        continue
                for blob in current_diff:
                    self.repo_issues += self.diff_worker(blob, prev_commit)
                # avoid searching the same diffs
                already_searched.add(diff_hash)
                prev_commit = commit
                self.output['commit_count'] += 1
            # if not prev_commit, then commit is the newest commit. And we have nothing to diff with.
            # But we will diff the first commit with NULL_TREE here to check the oldest code.
            # In this way, no commit will be missed. (7/26 except bug when using max_depth; fixed below)
            if total_commits:
                for first_commit in tmp_repo.iter_commits(skip=total_commits - 1):
                    current_diff = first_commit.diff(NULL_TREE, create_patch=True)
                    for blob in current_diff:
                        self.repo_issues += self.diff_worker(blob, prev_commit)
                    self.output['commit_count'] += 1
            else:
                current_diff = commit.diff(NULL_TREE, create_patch=True)
                for blob in current_diff:
                    self.repo_issues += self.diff_worker(blob, prev_commit)
                self.output['commit_count'] += 1

        logger.debug("Diff worker found %s issues on all branches" % len(self.repo_issues))
        # self cleanup of local cloned files
        self.cleanup()
        if self.whitelist_all:
            self.dump_whitelist()
        self.logger.debug("Find Strings complete! \n")
        return True

    def diff_worker(self, blob, prev_commit):
        diff_results = []
        file_path = blob.b_path or blob.a_path
        # skip 3rd party code committed in error, like 'node_modules' for efficiency increase
        if not any(match in file_path for match in self.ignores):
            printable_diff = blob.diff.decode('utf-8', errors='replace')
            if printable_diff.startswith("Binary files"):
                return diff_results
            pc_time = datetime.datetime.fromtimestamp(prev_commit.committed_date).strftime(
                '%Y-%m-%d %H:%M:%S')
            pc_hash = prev_commit.hexsha
            pc_message = prev_commit.message
            if self.do_entropy:
                diff_results += self.find_entropy(printable_diff, file_path, pc_hash, pc_time, pc_message)
            if self.do_regex:
                diff_results += self.regex_check(printable_diff, file_path, pc_hash, pc_time, pc_message)
        return diff_results

    def parse_results(self):
        if self.repo_issues:
            paths_found = []
            for issue in self.repo_issues:
                path = issue['path']
                if path not in paths_found:
                    paths_found.append(path)
                commit_hashes = issue['commit_hash']
                if self.whitelist_all:
                    t = (str(commit_hashes), self.repo_name)
                    if t not in self.repo_hash:
                        self.repo_hash.append(t)

                # truncate all diff findings, so hogs aren't logged
                issue['print_diff'] = issue['print_diff'][:12] + 'XXXXXXX'
                issue['commit'] = issue['commit'][:64] + '...'

                if not self.matched_whitelist(commit_hashes, self.repo_name):
                    if self.output_json:
                        print((json.dumps(issue, sort_keys=True)))
                    else:
                        finding = {'branch': [], 'commit': [], 'print_diff': [], 'reason': [], 'date': [],
                                   'commit_hash': []}
                        self.finding_summaries.setdefault(path, finding)
                        # group verbose hogs finding, only reference multiple branches/other finding is in
                        for key in finding.keys():
                            if issue[key] not in self.finding_summaries[path][key]:
                                self.finding_summaries[path][key].append(issue[key])
                else:
                    self.whitelist_count += 1
            # pack findings into dict
            for path in self.finding_summaries.keys():
                collapsed_finding = {path: self.finding_summaries[path]}
                self.findings.append(collapsed_finding)

    def save_results(self):
        # Write out to local file system for manual reporting or aggregation, and dup print to terminal
        ###############
        if self.findings:
            st = datetime.datetime.now().strftime('%Y-%m-%d_%H')
            secrets_working_dir = 'scans_' + st
            scan_project = self.project or self.repo_name.rstrip('/').split('/')[-1]
            scan_dir = secrets_working_dir + '/' + scan_project + '_scan_' + str(st)

            if not os.path.exists(secrets_working_dir):
                os.makedirs(secrets_working_dir)
            if not os.path.exists(scan_dir):
                os.makedirs(scan_dir)
            working_scan_path = scan_dir + '/'

            for repo, paths in list(self.findings.items()):
                if "file:///" in repo:
                    repo_string = repo.split('/')[-2]
                else:
                    repo_string = repo.rstrip('/').split('/')[-1]
                file_out = repo_string
                try:
                    with open(working_scan_path + file_out, "w+") as out:
                        repo_break = '*' * 80 + '\n'
                        out.write(repo_break)
                        out.write('Potential findings for repo %s \n' % self.repo_name)
                        out.write(repo_break)
                        delim = '-' * 80 + '\n'
                        annotate(delim, out)
                        for path, md in list(paths.items()):
                            annotate("\n!!! ******** File Path ********** !!! %s " % path, out)
                            print_finding(md, out)
                except IOError:
                    logger.info("Error opening path to write findings for repo %s" % repo)

    def print_results(self):
        if self.findings:
            annotate(self.debug_break, verbose=True)
            annotate("\n Possible findings for repo %s \n" % self.repo_name, verbose=True)
            annotate(self.debug_break, verbose=True)
            for finding in self.findings:
                for key in finding.keys():
                    annotate("\n!!! ******** File Path ********** !!! %s " % key, verbose=True)
                    print_finding(finding[key], verbose=True)
        else:
            logger.info("No results found for repo %s" % self.repo_name)

    def ci_summary(self):
        if self.findings:
            hashes = []
            annotate(" [.] %s files flagged for possible secrets in %s!" % (len(self.findings), self.repo_name),
                     verbose=True)
            for finding in self.findings:
                for filename in finding.keys():
                    logger.info(" Flagged: %s" % filename)
                    logger.info("     Reason(s): %s" % ", ".join(map(str, finding[filename]['reason'])))
            logger.info(
                " Summary only. Use --ci_hashes for detail, and see CSP clover UI for detailed async scan results \n")

    def ci_hashes(self):
        if self.findings:
            annotate(" [.] %s files flagged for possible secrets in %s!" % (len(self.findings), self.repo_name),
                     verbose=True)
            for finding in self.findings:
                for filename in finding.keys():
                    logger.info(" Flagged: %s" % filename)
                    logger.info("    Reason(s): %s" % ", ".join(map(str, finding[filename]['reason'])))
                    logger.info("    Offending commit Hashes for this file:")
                    for c_hash in finding[filename]['commit_hash']:
                        logger.info("    %s" % c_hash)

    def cleanup(self):
        if self.temp_repo_path:
            self.check_specific_files()
            shutil.rmtree(self.temp_repo_path, onerror=self.del_rw)
            self.logger.debug("Removed local repo at %s" % self.temp_repo_path)
            self.temp_repo_path = None
        else:
            logger.info("Scanner may have had an issue, nothing to cleanup or report")

    @staticmethod
    def del_rw(action, name, exc):
        os.chmod(name, stat.S_IWRITE)
        os.remove(name)

    @staticmethod
    def shannon_entropy(data, iterator):
        """
        Borrowed from http://blog.dkbza.org/2007/05/scanning-data-for-entropy-anomalies.html
        """
        if not data:
            return 0
        entropy = 0
        for x in iterator:
            p_x = float(data.count(x)) / len(data)
            if p_x > 0:
                entropy += - p_x * math.log(p_x, 2)
        return entropy

    @staticmethod
    def get_strings_of_set(word, char_set, threshold=20):
        count = 0
        letters = ""
        strings = []
        for char in word:
            if char in char_set:
                letters += char
                count += 1
            else:
                if count > threshold:
                    strings.append(letters)
                letters = ""
                count = 0
        if count > threshold:
            strings.append(letters)
        return strings

    # possibly useful to class to validate public/private status on a static repo_url
    @staticmethod
    def get_repo_md(url, vcs=None):
        url_headers = None
        if vcs == 'bitbucket':
            try:
                un = os.environ['bb_serv_user']
                pw = os.environ['bb_serv_pw']
                b64cred = base64.encodestring('%s:%s' % (un, pw)).replace('\n', '')
                url_headers = {'Authorization': 'Basic %s' % b64cred}
            except KeyError:
                logger.info("BB API token or creds not found in env var 'bb_serv_user, bb_serv_pw', quitting")
                sys.exit(1)

        elif vcs == 'github':
            try:
                url_headers = {'Authorization': 'token %s' % os.environ['git_api']}
            except KeyError:
                logger.info("Git API token not found in env var 'git_api' or BB, trying api unauthenticated")
        else:
            return requests.get(url)

        return requests.get(url, headers=url_headers)

    def dump_whitelist(self):
        outfile = "secrets_scan_whitelist_" + datetime.datetime.now().strftime('%Y-%m-%d_%H')
        with open(outfile, 'w') as file_handler:
            for item in self.repo_hash:
                file_handler.write("{}\n".format(item))

    def matched_whitelist(self, commit_hash, repo):
        if self.whitelist_in[commit_hash]:
            if repo in self.whitelist_in[commit_hash]:
                return True
        return False

    def clone_git_repo(self):
        try:
            self.temp_repo_path = tempfile.mkdtemp()
        except Exception as e:
            logger.error("Error creating mktmp dir at: %s" % self.temp_repo_path)
            logger.error(e)
            return None
        # Thread clone of repo. quit after 120 - and report error with repo
        if self.repo_token_url:
            async_clone = Process(target=Repo.clone_from, args=(self.repo_token_url, self.temp_repo_path))
        elif self.repo_ssh:
            async_clone = Process(target=Repo.clone_from, args=(self.repo_ssh, self.temp_repo_path))
        else:
            async_clone = Process(target=Repo.clone_from, args=(self.repo_url, self.temp_repo_path))
        try:
            async_clone.start()
            async_clone.join(self.clone_timeout)
            if async_clone.is_alive():
                logger.error(f"[.]CLONE TIMEOUT: {self.repo_url} over {self.clone_timeout / 60} min; abort. tell Dave!")
                async_clone.terminate()
                async_clone.join()
                self.cleanup()
                return None
        except exc.GitCommandError:
            logger.error("Error cloning supplied repo %s, check url" % self.repo_url)
            return None
        except Exception as e:
            logger.error('Threaded clone had error: had error: %s' % e)
            return None
        logger.debug('Find Strings: Git clone for %s successful to path %s' % (self.repo_url, self.temp_repo_path))
        return True

    def check_specific_files(self):
        to_check = ['security.md', 'security.MD', 'SECURITY.md', '.SECURITY.MD', 'SECURITY.MD']
        root_list = os.scandir(self.temp_repo_path)
        if any(md_file in root_list for md_file in to_check):
            self.security_md_exists = True

    def check_repo_size(self, repo_path=None):
        total = 0
        try:
            for entry in os.scandir(repo_path):
                if entry.is_file():
                    total += entry.stat().st_size
                elif entry.is_dir():
                    total += self.check_repo_size(entry.path)
        except NotADirectoryError:
            return os.path.getsize(repo_path)
        except PermissionError:
            return 0
        return total

    @staticmethod
    def get_m_stat():
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss


# python package management sucks so adding these directly in :/
def annotate(message, out=None, verbose=None):
    if verbose and len(message) > 1:
        logger.info(message)
    if out:
        try:
            out.write(message + '\n')
        except UnicodeEncodeError:
            logger.info("Unicode string exception :/")
        except:
            logger.info("Sorry, couldn't write that")


def print_finding(md, out=None, verbose=None):
    annotate("[ Reason(s) for possible match ] ", out, verbose)
    for r in md['reason']:
        annotate("    " + r, out, verbose)
    annotate("[ Branches that match was identified ] ", out, verbose)
    for b in md['branch']:
        annotate("    " + b, out, verbose)
    annotate("[ Truncated snippet(s) of regex line/string match ] ", out, verbose)
    for s in md['print_diff']:
        annotate("    %s " % s, out, verbose)
    annotate("[ Commit comments ]", out, verbose)
    for c in md['commit']:
        annotate("    " + c, out, verbose)
    annotate("[ Previous Commit hashes (finding compared to) ]", out, verbose)
    for h in md['commit_hash']:
        annotate("    " + h, out, verbose)
    annotate("[ Commit dates ]", out, verbose)
    for d in md['date']:
        annotate("    " + d, out, verbose)


regex_patterns = {
    "AWS private key possible match": "[A-Z0-9_]+ [ = ] [A-Z0-9]{39}.$",
    "Slack Token": "(xox[p|b|o|a]-[0-9]{12}-[0-9]{12}-[0-9]{12}-[a-z0-9]{32})",
    "RSA private key": "-----BEGIN RSA PRIVATE KEY-----",
    "SSH (OPENSSH) private key": "-----BEGIN OPENSSH PRIVATE KEY-----",
    "GitHub": "[g|G][i|I][t|T][h|H][u|U][b|B].*['|\"][0-9a-zA-Z]{35,40}['|\"]",
    "Google Oauth": "(\"client_secret\":\"[a-zA-Z0-9-_]{24}\")",
    "Heroku API Key": "[h|H][e|E][r|R][o|O][k|K][u|U].*[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}",
    "Generic Secret": "[s|S][e|E][c|C][r|R][e|E][t|T].*['|\"][0-9a-zA-Z]{32,45}['|\"]",
    "Possible hardcoded passwords": "PASSWORD.{1,20}(=|:)"
}


# configuration to be run as a command line script
def main():
    memory_conv = 1000000
    parser = argparse.ArgumentParser(description='Find secretshog hidden in the depths of git.')
    parser.add_argument('--json', dest="output_json", action="store_true", help="Output in JSON")
    parser.add_argument("--do_regex", dest="do_regex", action="store_true", help="Enable high signal regex checks")
    parser.add_argument("--dry_run ", dest="dry_run", action="store_true", help="Setup repos but dont clone or scan",
                        default=False)
    parser.add_argument("--skip_tests", dest="skip_tests", action="store_true",
                        help="Dont report on test file findings")
    parser.add_argument("--rules", dest="rules", help="Ignore default regexes and source from json list file",
                        default='regexes.json')
    parser.add_argument("--do_entropy", dest="do_entropy", action="store_true", help="Enable entropy checks")
    parser.add_argument("--since_commit", dest="since_commit", help="Only scan from a given commit hash", default=None)
    parser.add_argument("--max_depth", dest="max_depth",
                        help="The max commit depth to go back when searching for secretshog", default=864)
    parser.add_argument("--debug", dest="print_debug", action="store_true", help="Enable debug output", default=False)
    parser.add_argument('--repo_url', dest="repo_url", help='URL for secret searching')
    parser.add_argument('--whitelist_all', dest="whitelist_all", action="store_true",
                        help="Create file named <input> storing list of findings to whitelist on subsequent runs")
    parser.add_argument('--whitelist_file', dest="whitelist_file", help='File with hashes to skip', default=None)

    parser.add_argument("--max_threads", dest="max_threads", help="Thread count for parallel scanning", default=24)
    parser.add_argument("--save", dest="save", action="store_true", help="Save results to local file")
    parser.add_argument("--print_findings", dest="print_findings", action="store_true", help="Print results to screen")
    parser.add_argument("--ci", dest="ci", action="store_true", help="Print a short summary for CI PASS/FAIL")
    parser.add_argument("--ci_hashes", dest="ci_hashes", action="store_true", help="Print semi detailed CI PASS/FAIL")

    args = parser.parse_args()

    if args.print_debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if args.print_debug:
        usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        logger.debug("SECRETS HOG Main: MEM: Hogs Search on startup - mem use:")
        logger.debug(usage / memory_conv)

    ######################################################
    # Single repo run
    ######################################################
    if args.repo_url:
        r = SecretsHog(repo_url=args.repo_url, max_depth=args.max_depth, whitelist_file=args.whitelist_file)
        logger.info(' Starting secrets search of %s ..' % r.repo_name)

        # Comparing search items to a whitelist, using default dict for hopeful performance if needed
        if args.whitelist_file:
            whitelist = []
            with open(args.whitelist_file, 'r') as w_in:
                for line in w_in.readlines():
                    whitelist.append(eval(line.rstrip('\n')))
            for k, v in whitelist:
                r.whitelist_in[k].append(v)
        if args.skip_tests:
            r.ignores.append('test')
        logger.debug(' Starting scan; commits at depth of %s \n' % args.max_depth)

        r.find_strings()
        if r.repo_issues:
            r.parse_results()
            if args.print_findings:
                r.print_results()
            if args.save:
                r.save_results()
            if args.ci:
                r.ci_summary()
            elif args.ci_hashes:
                r.ci_hashes()
            logger.info("[.] WARNING: Potential secrets flagged! Please check commits for cleartext SSH keys, passwords.")
        else:
            logger.info("OK: No findings for repo scan.")
    else:
        logger.info("No repo url or parent projects supplied, try again")


if __name__ == "__main__":
    main()
